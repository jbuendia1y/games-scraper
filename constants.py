PAGES = [
    {
        "name": "EPIC_GAMES",
        "url": "https://store.epicgames.com/es-ES/browse",
        "base_url": "https://store.epicgames.com"
    },
    {
        "name": "ORIGIN",
        "url": "https://www.origin.com/mex/en-us/store/browse",
        "base_url": "https://www.origin.com",
    }
]
