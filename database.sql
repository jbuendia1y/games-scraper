CREATE TABLE `distributors`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50),
    `display_name` VARCHAR(50),
    `base_url` VARCHAR(150),
    PRIMARY KEY(`id`)
);

INSERT
INTO `distributors`(`name`,`display_name`,`base_url`)
VALUES
(
`ORIGIN`,
`Origin`,
`https://www.origin.com`
),
(
`EPIC_GAMES`,
`Epic Games`,
`https://store.epicgames.com`
);

CREATE TABLE `genders`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    PRIMARY KEY(`id`)
);


CREATE TABLE `platforms`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    PRIMARY KEY(`id`)
);

CREATE TABLE `games`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    `description` TEXT,
    `price` DECIMAL(5,2),
    `image_url` TEXT,
    `original_page_id` INT,
    KEY `original_page_id_idx`(`original_page_id`),
    PRIMARY KEY(`id`)
);

CREATE TABLE `game_prices`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `game_id` INT,
    `price` DECIMAL(5,2),
    `distributor_id` INT,
    KEY `distributor_id_idx`(`distributor_id`),
    KEY `game_id_idx`(`game_id`),
    PRIMARY KEY(`id`)
);