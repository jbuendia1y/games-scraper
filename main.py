import constants
import pages_scraper
from utils.save_on_excel import save_on_excel

from repositories import games_repository, genders_repository, platform_repository
from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


@app.get("/", response_class=HTMLResponse)
def home(request: Request):
    return templates.TemplateResponse("index.html", {
        "request": request
    })


@app.get("/games")
def find_all_games():
    games = games_repository.find_all()
    return games


@app.get("/games/{game_id}")
def find_one_game(game_id: int):
    game = games_repository.find_one(game_id=game_id)
    return game


@app.get("/genders")
def find_all_genders():
    genders = genders_repository.fetch_all()
    return genders


@app.get("/genders/{gender_id}")
def find_one_gender(gender_id: int):
    gender = genders_repository.fetch_one(id=gender_id)
    return gender


def main():
    pages_scraper.EpicGames(constants.PAGES[0]).run()

    games = games_repository.find_all()
    save_on_excel(games, "GAMES")


if __name__ == '__main__':
    main()
