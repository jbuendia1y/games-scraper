# Games scraper 

This project currently gets data from pages like :

- EpicGames

The project provides a **free, non-profit** API.

All the data obtained is saved in an excel and in a database.

The documentation about the API is in [docs](https://games-scraper-api-production.up.railway.app/docs)

The project is built with technologies such as:
- Selenium(Chrome)
- Beautifulsoup4
- Pandas
- FastAPI
- PyMysql