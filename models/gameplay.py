from dataclasses import dataclass


@dataclass
class Gameplay:
    id: int
    game_id: int
    min_players: int
    max_players: int
    online: bool
