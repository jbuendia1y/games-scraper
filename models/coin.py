from dataclasses import dataclass


@dataclass
class Coin:
    id: int
    name: str
