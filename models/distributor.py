from dataclasses import dataclass


@dataclass
class Distributor:
    id: int
    name: str
    display_name: str
    base_url: str