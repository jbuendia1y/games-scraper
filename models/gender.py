from dataclasses import dataclass


@dataclass
class Gender:
    id: int
    name: str
