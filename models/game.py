from dataclasses import dataclass, field

from models.original_page import OriginalPage
from models.gender import Gender
from models.platform import Platform
from typing import List


@dataclass
class Game:
    id: int
    name: str
    description: str
    price: int
    image_url: str
    original_page: OriginalPage
    genders: List[Gender]
    platforms: List[Platform]


