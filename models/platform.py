from dataclasses import dataclass


@dataclass
class Platform:
    id: int
    name: str
