from utils.scraper import scraper
from utils import console
from utils.get_regex import get_price, get_numbers
from bs4 import BeautifulSoup
from bs4.element import PageElement
from repositories import games_repository

from typing import List


class EpicGames:
    page: dict

    def __init__(self, page: dict):
        self.page = page

    def get_paginated_pages(self) -> List[str]:
        """ Return a urls list """
        print("FETCHING PAGINATED PAGES ...")
        driver = scraper(self.page['url'])
        soup = BeautifulSoup(driver.page_source, 'lxml')

        # Get max_items on url
        [max_items] = get_numbers(driver.current_url)

        pagination_els_items = soup.find_all(name="a", attrs={
            "data-component": "PaginationItem"
        })

        # len() - 2
        # Because the last item is an arrow
        last_pagination_item = int(pagination_els_items[len(pagination_els_items) - 2].text)

        # Here create a list of numbers
        n_items = [item * max_items for item in range(last_pagination_item)]
        # Creating a list of urls
        urls = [f"https://store.epicgames.com/es-ES/browse?sortBy=releaseDate&sortDir=DESC&count=40&start={start_attr}"
                for start_attr in n_items]
        print("----- OBTAINED PAGINATED PAGES ! -----")
        driver.close()
        return urls

    def scrap_game(self, item: PageElement):
        console.separate_line()
        el_image = item.find(name="img")
        image = el_image['src']
        title = el_image['alt']
        link = item.find(name="a")['href']

        price_layout = item.find(name="div", attrs={
            "data-component": "PriceLayout"
        })

        if price_layout is None:
            return

        el_prices = price_layout.find_all(name="span", attrs={
            "data-component": "Text"
        })

        price = get_price(el_prices[len(el_prices) - 1].text.split(' ')[0])

        one_driver = scraper(f"{self.page['base_url']}{link}")
        game_html_code = one_driver.page_source
        game_soup = BeautifulSoup(game_html_code, 'lxml')

        product_section_el = game_soup.find(name="div", attrs={
            "data-component": "ProductSection"
        })

        description = ""
        if product_section_el is not None:
            description = product_section_el.find(name="span", attrs={
                "data-component": "Text"
            }).text

        games_repository.create({
            "name": title,
            "description": description,
            "price": price,
            "image_url": image,
        })

        one_driver.close()
        console.separate_line()

    def run(self):
        urls = self.get_paginated_pages()
        for url in urls:
            print(f"##### OPENING {url} #####")
            driver = scraper(url)
            html_code = driver.page_source

            soup = BeautifulSoup(html_code, 'lxml')

            items = soup.find(name="section", attrs={
                "data-component": "BrowseGrid"
            }).find_all("li")

            for item in items:
                try:
                    self.scrap_game(item)
                except AttributeError as e:
                    print(e)
            print("##### CLOSING PAGE #####")
            driver.close()
