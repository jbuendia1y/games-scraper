from utils.mysql_connection import mysql_connection
from models.platform import Platform


def create_platform_addapted(row):
    return Platform(
        id=row["id"],
        name=row["name"]
    )


def create(data):
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute(
        'INSERT INTO platforms(name) VALUES (%s)', (data["name"])
        )
    row = cursor.fetchone()
    cursor.close()

    connection.commit()
    connection.close()

    return create_platform_addapted(row)


def fetch_all():
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM platforms")
    rows = cursor.fetchall()
    cursor.close()

    connection.commit()
    connection.close()

    return [create_platform_addapted(row) for row in rows]


def fetch_one(id:int):
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM platforms WHERE id=%s", id)
    row = cursor.fetchone()
    cursor.close()

    connection.commit()
    connection.close()

    return create_platform_addapted(row)


def fetch_one_by_name(name:str):
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM platforms WHERE name=%s", name)
    row = cursor.fetchone()
    cursor.close()

    connection.commit()
    connection.close()

    return create_platform_addapted(row)

