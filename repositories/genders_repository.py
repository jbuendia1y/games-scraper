from utils.mysql_connection import mysql_connection
from models.gender import Gender


def create_gender_addapted(row):
    return Gender(
        id=row["id"],
        name=row["name"]
    )


def create(data):
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("INSET INTO genders(name) VALUES (%s)", (
        data["name"]
    ))

    row = cursor.fetchone()
    cursor.close()

    connection.commit()
    connection.close()

    return create_gender_addapted(row)


def fetch_all():
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM genders")
    rows = cursor.fetchall()
    cursor.close()

    connection.commit()
    connection.close()

    return [create_gender_addapted(row) for row in rows]


def fetch_one(id: int):
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM genders WHERE id=%s", id)
    row = cursor.fetchone()
    cursor.close()

    connection.commit()
    connection.close()

    return create_gender_addapted(row)


def fetch_one_by_name(name: str):
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM platforms WHERE name=%s", name)
    row = cursor.fetchone()
    cursor.close()

    connection.commit()
    connection.close()

    return create_gender_addapted(row)
