from utils.mysql_connection import mysql_connection
from models.game import Game


def created_game_addapted(row):
    data = {
        "id": row["id"],
        "name": row["name"],
        "description": row["description"],
        "price": row["price"],
        "image_url": row["image_url"],
        "original_page_id": row["original_page_id"]
    }
    return data


def create(data):
    print("CREATING A GAME")
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute(
        'INSERT INTO games(name, description,price ,image_url, original_page_id) VALUES (%s,%s,%s,%s,%s)', (
            data["name"],
            data["description"],
            data["price"],
            data["image_url"],
            getattr(data, "original_page_id", None),
        ))
    cursor.close()

    connection.commit()
    connection.close()

    print(f"CREATED GAME Nº{cursor.lastrowid} !")


def update(data):
    print(f"UPDATING GAME Nº{data['id']}")


def find_all():
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM games")
    data = [created_game_addapted(row) for row in cursor.fetchall()]
    cursor.close()

    connection.close()
    return data


def find_one(game_id: int):
    connection = mysql_connection()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM games WHERE id=%s", game_id)
    row = cursor.fetchone()
    cursor.close()

    connection.close()
    return created_game_addapted(row)
