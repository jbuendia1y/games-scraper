import re

free_texts = [
    "GRATIS",
    "FREE",
]


def get_numbers(text: str):
    results = re.search(r'([0-9]{1,3})', text)
    return [int(result) for result in results.groups()]


def get_price(text: str):
    if text.upper().strip() in free_texts:
        return float(0.00)
    results = re.search(r'([0-9,]+(\.[0-9]{2})?)', text)
    print(results.groups())
    return float(results.group(1).replace(",", "."))
