import pymysql
from pymysql.cursors import DictCursor
import os


def database_envs():
    return {
        "host": os.environ.get("SQL_HOST"),
        "user": os.environ.get("SQL_USER"),
        "password": os.environ.get("SQL_PASSWORD"),
        "database": os.environ.get("SQL_DATABASE"),
    }


def mysql_connection():
    return pymysql.connect(
        **database_envs(),
        cursorclass=DictCursor,
        ssl={
            'key': "AnyKey"
        }
    )
