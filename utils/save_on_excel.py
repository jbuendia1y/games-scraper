import pandas as pd
import datetime


def save_on_excel(data: dict, sheet_name: str):
    """ Save data on Excel file """
    print("READING DATA ...")
    df = pd.DataFrame(data)
    date = datetime.date.today().__str__()

    folder = "datasets"
    filename = f"game_dataset-{date}.xlsx"

    df.to_excel(f"{folder}/{filename}", sheet_name=sheet_name)
    print(f"DATA SAVED ON {folder}")
