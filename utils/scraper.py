from selenium import webdriver


def scraper(url: str = None):
    google_chrome_driver = webdriver.Chrome(
        executable_path="drivers/chromedriver")
    driver = google_chrome_driver

    if url:
        driver.get(url)

    return driver
