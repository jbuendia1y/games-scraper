import urllib.request


def get_html_of_page(url: str):
    # * Return html of page
    page = urllib.request.urlopen(url)

    html = page.read().decode("utf8")
    page.close()
    return html
